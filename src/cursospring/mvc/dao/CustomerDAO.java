package cursospring.mvc.dao;

import java.util.List;

import cursospring.mvc.dominio.Customer;

public interface CustomerDAO {

	Long getCount();
	
	Customer get(Long id);
	
	List<Customer> getAll();
	
	void insert(Customer customer);
	
	void update(Customer customer);
	
	void delete(Customer customer);
	
}
