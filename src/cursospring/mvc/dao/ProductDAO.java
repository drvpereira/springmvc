package cursospring.mvc.dao;

import java.util.List;

import cursospring.mvc.dominio.Product;


public interface ProductDAO {

	Long getCount();
	
	Product get(Long id);
	
	List<Product> getAll();
	
	void insert(Product product);
	
	void update(Product product);
	
	void delete(Product product);
	
}
