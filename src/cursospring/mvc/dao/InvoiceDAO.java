package cursospring.mvc.dao;

import java.util.List;

import cursospring.mvc.dominio.Invoice;

public interface InvoiceDAO {

	Long getCount();
	
	Invoice get(Long id);
	
	List<Invoice> getAll();
	
	void insert(Invoice invoice);
	
	void update(Invoice invoice);
	
	void delete(Invoice invoice);
	
		
}
