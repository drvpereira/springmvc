package cursospring.mvc.dao;

import java.util.List;

import cursospring.mvc.dominio.Item;

public interface ItemDAO {

	Long getCount();
	
	Item get(Long invoiceId, Long productId);
	
	List<Item> getAll();
	
	void insert(Item item);
	
	void update(Item item);
	
	void delete(Item item);
	
}
