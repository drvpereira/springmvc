package cursospring.mvc.dao.hibernate;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import cursospring.mvc.dao.ProductDAO;
import cursospring.mvc.dominio.Product;

@Component @Transactional
public class ProductDAOHibernate implements ProductDAO {

	@Autowired
	private SessionFactory sf;

	@Override
	public Long getCount() {
		return (Long) sf.getCurrentSession().createQuery("select count(p) from Product p").uniqueResult();
	}

	@Override
	public Product get(Long id) {
		return (Product) sf.getCurrentSession().get(Product.class, id);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Product> getAll() {
		return sf.getCurrentSession().createCriteria(Product.class).list();
	}

	@Override
	public void insert(Product product) {
		sf.getCurrentSession().save(product);
	}

	@Override
	public void update(Product product) {
		sf.getCurrentSession().update(product);
	}

	@Override
	public void delete(Product product) {
		sf.getCurrentSession().delete(product);
	}
}
