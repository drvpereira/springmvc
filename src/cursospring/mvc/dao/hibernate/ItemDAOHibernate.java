package cursospring.mvc.dao.hibernate;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import cursospring.mvc.dao.ItemDAO;
import cursospring.mvc.dominio.Item;

@Component @Transactional
public class ItemDAOHibernate implements ItemDAO {

	@Autowired
	private SessionFactory sf;

	@Override
	public Long getCount() {
		return (Long) sf.getCurrentSession().createQuery("select count(i) from Item i").uniqueResult();
	}

	@Override
	public Item get(Long invoiceId, Long productId) {
		return (Item) sf.getCurrentSession().createQuery("select i from Item i where i.invoice.id = ? and i.product.id = ?")
				.setLong(0, invoiceId).setLong(1, productId).uniqueResult();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Item> getAll() {
		return sf.getCurrentSession().createCriteria(Item.class).list();
	}

	@Override
	public void insert(Item item) {
		sf.getCurrentSession().save(item);
	}

	@Override
	public void update(Item item) {
		sf.getCurrentSession().update(item);
	}

	@Override
	public void delete(Item item) {
		sf.getCurrentSession().delete(item);
	}
	
}

