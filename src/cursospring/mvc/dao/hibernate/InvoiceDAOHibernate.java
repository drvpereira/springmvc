package cursospring.mvc.dao.hibernate;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import cursospring.mvc.dao.InvoiceDAO;
import cursospring.mvc.dominio.Invoice;

@Component @Transactional
public class InvoiceDAOHibernate implements InvoiceDAO {
	
	@Autowired
	private SessionFactory sf;

	@Override
	public Long getCount() {
		return (Long) sf.getCurrentSession().createQuery("select count(i) from Invoice i").uniqueResult();
	}

	@Override
	public Invoice get(Long id) {
		return (Invoice) sf.getCurrentSession().get(Invoice.class, id);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Invoice> getAll() {
		return sf.getCurrentSession().createCriteria(Invoice.class).list();
	}

	@Override
	public void insert(Invoice invoice) {
		sf.getCurrentSession().save(invoice);
	}

	@Override
	public void update(Invoice invoice) {
		sf.getCurrentSession().update(invoice);
	}

	@Override
	public void delete(Invoice invoice) {
		sf.getCurrentSession().delete(invoice);
	}
}

