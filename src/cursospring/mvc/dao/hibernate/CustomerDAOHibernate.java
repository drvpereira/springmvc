package cursospring.mvc.dao.hibernate;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import cursospring.mvc.dao.CustomerDAO;
import cursospring.mvc.dominio.Customer;

@Component @Transactional
public class CustomerDAOHibernate implements CustomerDAO {
	
	@Autowired
	private SessionFactory sf;

	@Override
	public Long getCount() {
		return (Long) sf.getCurrentSession().createQuery("select count(c) from Customer c").uniqueResult();
	}

	@Override
	public Customer get(Long id) {
		return (Customer) sf.getCurrentSession().get(Customer.class, id);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Customer> getAll() {
		return sf.getCurrentSession().createCriteria(Customer.class).list();
	}

	@Override
	public void insert(Customer customer) {
		sf.getCurrentSession().save(customer);		
	}

	@Override
	public void update(Customer customer) {
		sf.getCurrentSession().update(customer);		
	}

	@Override
	public void delete(Customer customer) {
		sf.getCurrentSession().delete(customer);		
	}
}

