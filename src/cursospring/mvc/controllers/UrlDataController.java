package cursospring.mvc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/urlData")
public class UrlDataController {

	@RequestMapping("/reqParam")
	public void reqParam(@RequestParam int id) {
		System.out.println("Id enviado por parâmetro: " + id);
	}
	
	@RequestMapping("/pathVar/{id}")
	public void pathVar(@PathVariable int id) {
		System.out.println("Id enviado pela URL: " + id);
	}
	
}
