package cursospring.mvc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cursospring.mvc.dao.ProductDAO;
import cursospring.mvc.dominio.Product;

@Controller
@RequestMapping("/rest/product")
public class ProductRestController {

	@Autowired
	private ProductDAO dao;
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET, produces="application/json")
	public @ResponseBody Product getProduct(@PathVariable long id) {
		return dao.get(id);
	}
	
	@RequestMapping("/list")
	public String listaDinamica(Model model) {
		model.addAttribute("products", dao.getAll());
		return "productRest/list";
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public @ResponseBody String createProduct(@RequestBody Product product) {
		dao.insert(product);
		return String.valueOf(product.getId());
	}
	
	@RequestMapping(method=RequestMethod.PUT)
	public @ResponseBody String updateProduct(@RequestBody Product product) {
		return null;
	}

	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public @ResponseBody String deleteProduct(long id) {
		return null;
	}

}
