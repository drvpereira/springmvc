package cursospring.mvc.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cursospring.mvc.dao.CustomerDAO;
import cursospring.mvc.dominio.Customer;
import cursospring.mvc.validators.CustomerValidator;

@Controller
@RequestMapping("/customer")
public class CustomerController {
	
	@Autowired
	private CustomerDAO dao;

	@RequestMapping("/list")
	public String list(Model model) {
		List<Customer> customers = dao.getAll();
		model.addAttribute("customers", customers);
		return "customer/list";
	}

	@RequestMapping("/create")
	public String create(Model model) {
		model.addAttribute("customer", new Customer());
		return "customer/form";
	}

	@RequestMapping("/edit/{id}")
	public String create(@PathVariable long id, Model model) {
		model.addAttribute("customer", dao.get(id));
		return "customer/form";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(@ModelAttribute @Valid Customer customer, BindingResult result) {
		
		if (result.hasErrors()) {
			return "customer/form";
		} else {
			if (customer.getId() == null)
				dao.insert(customer);
			else
				dao.update(customer);
			
			return "redirect:list";
		}
		
	}
	
	@InitBinder
	private void initBinder(WebDataBinder binder) {
		binder.addValidators(new CustomerValidator());
	}

}
