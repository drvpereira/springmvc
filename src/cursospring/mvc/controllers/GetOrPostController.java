package cursospring.mvc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/customHttpMethod")
public class GetOrPostController {

	@RequestMapping(method=RequestMethod.GET)
	public String setupForm() {
		return "customHttpMethod/form";
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public String submitForm() {
		return "customHttpMethod/success";
	}
	
}
