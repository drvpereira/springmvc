package cursospring.mvc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller // Define que a classe é um controller
@RequestMapping("/exemplo")
public class ExemploController {

	// O método está associado à URL /exemplo/hello. 
	@RequestMapping("/hello")
	public String helloWorld() {
		return "hello"; // Exibirá a JSP /WEB-INF/views/hello.jsp
	}
	
}
