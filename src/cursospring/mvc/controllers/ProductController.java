package cursospring.mvc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;

import cursospring.mvc.dao.ProductDAO;
import cursospring.mvc.dominio.Product;

@Controller
public class ProductController {

	@Autowired
	private ProductDAO dao;
	
	public String list(Model model) {
		return null;
	}
	
	public String create(@PathVariable long id, Model model) {
		return null;
	}
	
	public String create() {
		return null;
	}
	
	public String edit(long id, Model model) {
		return null;
	}
	
	public String save(Product product) {
		return null;
	}
	
	public String update(Product product) {
		return null;
	}
	
	public String delete(long id) {
		return null;
	}
	
}
