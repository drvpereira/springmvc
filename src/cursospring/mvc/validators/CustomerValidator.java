package cursospring.mvc.validators;

import java.util.Date;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import cursospring.mvc.dominio.Customer;

public class CustomerValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return Customer.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		
		Customer c = (Customer) obj;
		
		ValidationUtils.rejectIfEmpty(errors, "firstName", "empty");
		ValidationUtils.rejectIfEmpty(errors, "lastName", "empty");
		ValidationUtils.rejectIfEmpty(errors, "birthDate", "empty");
		ValidationUtils.rejectIfEmpty(errors, "street", "empty");
		ValidationUtils.rejectIfEmpty(errors, "city", "empty");
		
		if (c.getBirthDate() != null && new Date().before(c.getBirthDate())) {
			errors.rejectValue("birthDate", "invalid");
		}
		
	}

}
