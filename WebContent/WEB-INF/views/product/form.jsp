<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<h1>Novo Produto</h1>

<form:form action="save" method="post" modelAttribute="product">

	Nome: <form:input path="name"/> <form:errors path="name"/> <br/>
	Pre�o: <form:input path="price"/> <form:errors path="price"/> <br/>
	
	<form:hidden path="id"/>
	<form:button type="submit">Salvar</form:button>
	
</form:form>
 