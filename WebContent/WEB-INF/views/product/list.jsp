<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<h1>Lista de Produtos</h1>

<table>
	<c:forEach items="${ products }" var="p">
		<tr>
			<td>${ p.id }</td>
			<td>${ p.name }</td>
			<td>${ p.price }</td>
		</tr>
	</c:forEach>
</table>