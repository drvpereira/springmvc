<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<h1>Novo Produto</h1>

<form:errors path="customer.*" />

<form:form action="${ pageContext.request.contextPath }/customer/save" method="post" modelAttribute="customer">

	Nome: <form:input path="firstName"/> <br/>
	
	Sobrenome: <form:input path="lastName"/> <br/>
	
	Nascimento: <form:input path="birthDate"/> <br/>
	
	Endere�o: <form:input path="street" /> <br/>
	
	Cidade: <form:input path="city" /> <br/>
	
	<form:hidden path="id"/>
	
	<form:button type="submit">Salvar</form:button>

</form:form>