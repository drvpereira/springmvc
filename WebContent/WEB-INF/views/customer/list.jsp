<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<h1>Lista de Clientes</h1>

<table>
	<c:forEach items="${ customers }" var="c">
		<tr>
			<td>${ c.id }</td>
			<td>${ c.firstName }</td>
			<td>${ c.lastName }</td>
			<td>${ c.birthDate }</td>
			<td>${ c.street }</td>
			<td>${ c.city }</td>
		</tr>
	</c:forEach>
</table>