<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<h1>Lista de Produtos</h1>

<table id="produtos">
	<tr>
		<th>Id</th>
		<th width="150">Nome</th>
		<th width="50">Pre�o</th>
		<th></th>
	</tr>
	
	<c:forEach items="${ products }" var="p">
		<tr>
			<td>${ p.id }</td>
			<td>${ p.name }</td>
			<td>${ p.price }</td>
			<td></td>
		</tr>
	</c:forEach>
</table>

<button id="novo">Novo Produto</button>

<script type="text/javascript" src="${ pageContext.request.contextPath }/resources/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">

	$('#novo').click(function() {
		$('#produtos').append('<tr id="dados"><td>#</td><td><input type="text" name="name"/></td><td><input type="text" name="price" size="10"/></td><td><button id="salvar">Salvar</button></td></tr>');
		$(this).hide();
		$('#salvar').click(function() {
			
			$.ajax({
				url: '${ pageContext.request.contextPath }/rest/product',
				type: 'post',
				data: JSON.stringify($('#dados').serializeJSON()),
				contentType: 'application/json'
			 }).done(function(data) {
				 $('#dados').remove();
				 
				 $.get('${ pageContext.request.contextPath }/rest/product/' + data, function(product) {
					$('#produtos').append('<tr><td>' + product.id + '</td><td>' + product.name + '</td><td>' + product.price + '</td></tr>');
					$('#novo').show();
				 }, 'json');
			 });
		});
	});
 </script>